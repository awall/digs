//
//  CardTableViewCell.swift
//  Digs
//
//  Created by Alex Wall on 6/21/18.
//  Copyright © 2018 Alex Wall. All rights reserved.
//

import UIKit
import Kingfisher

final class CardTableViewCell: UITableViewCell {
    
    private let containerView = UIView()
    private let largeImageView = UIImageView()
    private let titleLabel = UILabel()
    private let subtitleLabel = UILabel()
    private let locationBadgeLabel = UILabel()
    private let postTypeDot = UIView()
    private let profileImage = UIImageView()

    var post: TestPost? = nil {
        didSet {
            update()
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUpUI() {
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:))))
        
        contentView.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
        
        addSubview(containerView) { make in
            make.edges.equalToSuperview().inset(UIEdgeInsetsMake(9.2, 20.0, 9.2, 20.0))
        }
        let shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: containerView.bounds, cornerRadius: 12.0).cgPath
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = 12.0
        containerView.clipsToBounds = true
        shadowLayer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.7).cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        shadowLayer.shadowOpacity = 0.6
        shadowLayer.shadowRadius = 8
        
        containerView.layer.insertSublayer(shadowLayer, at: 0)
        
        
        setUpLabels()
        
        containerView.addSubview(largeImageView) { make in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(containerView.snp.width).multipliedBy(1.25)
        }
        largeImageView.sizeToFit()
        largeImageView.clipsToBounds = true
        largeImageView.addSubview(locationBadgeLabel) { make in
            make.bottom.equalTo(-13.0)
            make.right.equalTo(-11.0)
        }
        
        profileImage.apply(style: .profileImage)
        containerView.addSubview(profileImage) { make in
            make.top.equalTo(largeImageView.snp.bottom).offset(11.0)
            make.left.equalTo(8.0)
        }
        
        
        containerView.addSubview(titleLabel) { make in
            make.top.equalTo(largeImageView.snp.bottom).offset(10.0)
            make.left.equalToSuperview().offset(52.0)
            make.right.equalToSuperview().offset(-17.0)
        }
        
        postTypeDot.apply(style: .postTypeDot)
        containerView.addSubview(postTypeDot) { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(4.0)
            make.left.equalToSuperview().offset(56.0)
            make.bottom.equalToSuperview().offset(-12.0)
        }
        
        containerView.addSubview(subtitleLabel) { make in
            make.centerY.equalTo(postTypeDot.snp.centerY)
            make.left.equalTo(postTypeDot.snp.right).offset(4.0)
            make.right.equalToSuperview().offset(-17.0)
        }
    }
 
    private func setUpLabels() {
        titleLabel.apply(style: .cardTitle)
        subtitleLabel.apply(style: .subtitle)
        locationBadgeLabel.apply(style: .locationBadge)
    }
    
    private func update() {
        guard let post = post else { return }
        postTypeDot.backgroundColor = post.isRental ? UIColor.tangerine : UIColor.aquamarine
        titleLabel.text = post.addressText
        let price = NumberFormatter.localizedString(from: NSNumber(value: post.price), number: .currency)
        let priceUIString = "\(price)\(post.isRental ? " / mo" : "")"
        subtitleLabel.text = "\(priceUIString) • \(post.numberBedrooms) Bed • \(post.numberBathrooms) Bath"
        locationBadgeLabel.text = post.city
        
        largeImageView.kf.setImage(with: post.imageURL)
        profileImage.kf.setImage(with: post.profileImageURL)
    }
    
    @objc private func viewTapped(_ sender: UITapGestureRecognizer) {
        print("Card tapped")
    }
}
