//
//  Styles.swift
//  Digs
//
//  Created by Alex Wall on 6/18/18.
//  Copyright © 2018 Alex Wall. All rights reserved.
//

import UIKit


extension Style where Type == UIView {
    
    /// Badge
    static let postTypeDot: Style<UIView> = Style<UIView> { dot in
        dot.snp.makeConstraints { make in
            make.width.height.equalTo(8.0)
        }
        dot.layer.cornerRadius = 4.0
    }
    
}


extension Style where Type == UILabel {
    static let navTitle: Style<UILabel> = Style<UILabel> { label in
        label.font = .systemFont(ofSize: 27.0, weight: .regular)
        label.textColor = .textBlack
    }
    
    static let subtitle: Style<UILabel> = Style<UILabel> { label in
        label.font = .systemFont(ofSize: 11.0, weight: .light)
        label.textColor = .textGray
        label.numberOfLines = 1
    }
    
    static let cardTitle: Style<UILabel> = Style<UILabel> { label in
        label.font = .systemFont(ofSize: 17.0, weight: .light)
        label.textColor = .textGray
        label.numberOfLines = 1
    }
    
    static let locationBadge: Style<UILabel> = Style<UILabel> { label in
        label.font = .systemFont(ofSize: 11.0, weight: .light)
        label.textColor = .textWhite
        label.backgroundColor = .textGray
        label.layer.cornerRadius = 3.0
        label.clipsToBounds = true
        label.snp.makeConstraints { make in
            make.width.equalTo(95.0)
            make.height.equalTo(15.0)
        }
        label.numberOfLines = 0
        label.textAlignment = .center
    }
}

extension Style where Type == UIImageView {
    
    static let profileImage: Style<UIImageView> = Style<UIImageView> { imageView in
        imageView.snp.makeConstraints { make in
            make.height.width.equalTo(24.0)
        }
        imageView.layer.cornerRadius = 17.5
        imageView.clipsToBounds = true
    }
}

extension Style where Type == UINavigationBar {
    static let fade: Style<UINavigationBar> = Style<UINavigationBar> { bar in
        bar.barStyle = .default
        bar.tintColor = .textBlack
        bar.backgroundColor = .white
        
        bar.isTranslucent = false
        bar.layer.masksToBounds = false
        bar.shadowImage = CAGradientLayer(frame: CGRect(x: 0, y: 0, width: 1, height: 20), colors: UIColor.whiteFade).asImage()
    
        bar.titleTextAttributes = [.font: UIFont.systemFont(ofSize: 27.0, weight: .regular), .foregroundColor: UIColor.textBlack]
    }
    
}
