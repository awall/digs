//
//  AppDelegate.swift
//  Digs
//
//  Created by Alex Wall on 6/18/18.
//  Copyright © 2018 Alex Wall. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        let feedVC = ViewController()
        window!.rootViewController = UINavigationController(rootViewController: feedVC)
        window!.makeKeyAndVisible()
        UINavigationBar.appearance().apply(style: .fade)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) { }
    func applicationDidEnterBackground(_ application: UIApplication) { }
    func applicationWillEnterForeground(_ application: UIApplication) { }
    func applicationDidBecomeActive(_ application: UIApplication) { }
    func applicationWillTerminate(_ application: UIApplication) { }
}

