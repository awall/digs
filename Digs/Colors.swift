//
//  Colors.swift
//  Digs
//
//  Created by Alex Wall on 6/18/18.
//  Copyright © 2018 Alex Wall. All rights reserved.
//

import UIKit

extension UIColor {
    
    // Text
    static let textBlack = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let textWhite = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    static let textGray = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.7)
    
    // Badge
    static let aquamarine = #colorLiteral(red: 0.01960784314, green: 0.8352941176, blue: 0.6588235294, alpha: 1)
    static let tangerine = #colorLiteral(red: 1, green: 0.5921568627, blue: 0, alpha: 1)
    
    // Accents
    static let barneyPurple = #colorLiteral(red: 0.7647058824, green: 0.1450980392, blue: 0.4666666667, alpha: 1)
    static let veryOrange = #colorLiteral(red: 0.9568627451, green: 0.2431372549, blue: 0.1176470588, alpha: 1)
    // White fade
    static let whiteFade = [#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),#colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)]

}
