//
//  Utils.swift
//  Digs
//
//  Created by Alex Wall on 6/21/18.
//  Copyright © 2018 Alex Wall. All rights reserved.
//

import UIKit
import SnapKit

extension UIView {
    func addSubview(_ view: UIView, _ makeConstraints: (ConstraintMaker) -> Void) {
        addSubview(view)
        view.snp.makeConstraints(makeConstraints)
    }
}

extension CAGradientLayer {
    convenience init(frame: CGRect, colors: [UIColor]) {
        self.init()
        self.frame = frame
        self.colors = []
        for color in colors {
            self.colors?.append(color.cgColor)
        }
        
        startPoint = CGPoint(x: 0, y: 0)
        endPoint = CGPoint(x: 0, y: 1)
    }

    func asImage() -> UIImage? {
        var image: UIImage? = nil
        UIGraphicsBeginImageContext(bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        return image
    }
}
