//
//  Style.swift
//  Digs
//
//  Created by Alex Wall on 6/18/18.
//  Copyright © 2018 Alex Wall. All rights reserved.
//

import UIKit

struct Style<Type> {
    private let style: (Type) -> Void
    init(_ style: @escaping (Type) -> Void) {
        self.style = style
    }
    
    @discardableResult
    func apply(to view: Type) -> Type {
        style(view)
        return view
    }
    
    static func combine(_ styles: Style<Type>...) -> Style<Type> {
        return Style<Type> { (view: Type) in
            styles.forEach { style in
                style.apply(to: view)
            }
        }
    }
}

func + <Type>(lhs: Style<Type>, rhs: Style<Type>) -> Style<Type> {
    return Style.combine(lhs, rhs)
}

protocol Stylized { }
extension Stylized {
    @discardableResult
    func apply(style: Style<Self>) -> Self {
        return style.apply(to: self)
    }
}

extension UIView: Stylized {}
extension UIBarItem: Stylized {}
