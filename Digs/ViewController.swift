//
//  ViewController.swift
//  Digs
//
//  Created by Alex Wall on 6/18/18.
//  Copyright © 2018 Alex Wall. All rights reserved.
//

import UIKit

final class ViewController: UITableViewController {
    
    private var posts = [TestPost]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(CardTableViewCell.self, forCellReuseIdentifier: "CardCell")
        loadData()
        setUpUI()
    }

    private func setUpUI() {
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 500
        tableView.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
        tableView.allowsSelection = false
        
        title = "Explore"

        let addPostBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "iconNavigationCreatePost").withRenderingMode(.alwaysOriginal), style: .plain , target: self, action: #selector(addPostTapped(_:)))
        
        let profileImageButton = UIButton(type: .custom)
        profileImageButton.setImage(#imageLiteral(resourceName: "avatarUserpicJinYang"), for: .normal)
        profileImageButton.layer.cornerRadius = 17
        profileImageButton.clipsToBounds = true
        profileImageButton.snp.makeConstraints { make in
            make.width.height.equalTo(34.0)
        }
        profileImageButton.addTarget(self, action: #selector(profileButtonTapped(_:)), for: .touchUpInside)
        let profileBarButton = UIBarButtonItem(customView: profileImageButton)
        
        navigationItem.setLeftBarButton(addPostBarButton, animated: false)
        navigationItem.setRightBarButton(profileBarButton, animated: false)
    }
    
    private func loadData() {
        posts = TestPost.getTestFeed()
    }
    
    @objc private func addPostTapped(_ barButton: UIBarButtonItem) {
        print("Add a post tapped")
    }

    @objc private func profileButtonTapped(_ button: UIButton) {
        print("Profile Button Tapped")
    }
}


// Mark: - Table View Data Source and Delegate
extension ViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as? CardTableViewCell else { fatalError("Could not cast CardCell") }
        cell.post = posts[indexPath.row]
        return cell
    }
}
